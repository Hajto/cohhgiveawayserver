defmodule Cgas.PageController do
  use Cgas.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
