defmodule Cgas.GiveAwayMonitor do
  use Cgas.IrcServer,
    nick: "twitchsniperbot",
    password: "oauth:y34yipi127tevfrr27b2c5787hlb0m",
    channel: "#hajtosek"

  expect_message { _type, msg  ,
                   %ExIrc.SenderInfo{user: "cohhilitionbot"} ,
                   _channell}  do
    # IO.inspect "Message for processing: #{msg}"
    case check_message(msg) do
      { _ , map } -> Cgas.FCM.push_giveaway(map)
    end
  end

  def check_message(msg) do
    functions = Cgas.GiveAwayMessageCheck.__info__(:functions)
    Enum.reduce_while(functions, nil, fn {elem, 1} , _acc ->
      case apply(Cgas.GiveAwayMessageCheck, elem, [msg]) do
        nil -> {:cont, nil}
        tuple ->
          IO.inspect "Found message matching regex"
          IO.inspect tuple
          {:halt, tuple}
      end
    end)
  end

end
