defmodule Cgas.GiveAwayController do
  use Cgas.Web, :controller
  require Logger

  def enter(conn, _args) do
    Logger.debug inspect(get_session(conn, :login))
    %{"login" => login,
      "token" => token } = get_session(conn, :login)

    pid = Cgas.GiveAwayEntererStarter.enter(login,"oauth:"<> token)
    Logger.debug inspect(pid)


    json conn, %{"success" => "true "}
  end

end
