defmodule Cgas.AuthController do
  use Cgas.Web, :controller
  require Logger

  alias Cgas.TwitchAuth

  def auth(conn, _params) do
    redirect conn, external: TwitchAuth.authorize_url!
  end

  def callback(conn, %{ "code" => code}) do
    case TwitchAuth.get_token!(code: code) do
      nil ->
        conn
        |> put_status(:unauthorized)
        |> json( %{"error" => "Unauthorized, code probably expired"})
      value ->
        token = value.token.access_token
        parsed_response = Poison.decode!(OAuth2.Client.get!(value,
              "/user").body)
        chat_credentials = %{ "login" => parsed_response["name"],
                              "token" => token  }
        conn
        |> put_session(:login, chat_credentials)
        |> json(chat_credentials)
    end
  end

end
