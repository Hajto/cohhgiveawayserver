# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

config :oauth2,
  serializers: %{
    "application/vnd.api+json" => Poison
  }

# General application configuration
config :cgas,
  # ecto_repos: [Cgas.Repo],
  client_id: "8ab4pxvwgnwc8jgnuojvdtec2dxjdin",
  client_secret: "nrl3zn5oognxp40euvyp2bsivsza0ll",
  auth_server: "https://api.twitch.tv/kraken/oauth2/authorize",
  auth_callback: "http://cohhgas.ddns.net/auth/callback",
  site: "https://api.twitch.tv/kraken"

# Configures the endpoint
config :cgas, Cgas.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "v1HKDcEGXJBxaWzEdLK8WYMx0QbmVqE+PiWyOYqL2OwE+DR61KnvM8RBU4EZHCpD",
  render_errors: [view: Cgas.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Cgas.PubSub,
           adapter: Phoenix.PubSub.PG2]

config :exfcm,
  fcm_url: "https://fcm.googleapis.com/fcm/send",
  server_key: "AIzaSyAPxjR4K8gqiE3l51xg2TIEAfXfBqlVfxE"

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
