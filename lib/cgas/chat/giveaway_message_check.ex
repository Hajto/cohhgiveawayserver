defmodule Cgas.GiveAwayMessageCheck do

  @started ~r/^\s*\*\*\* NEW GIVEAWAY OPENED: \((.+?)\): Sponsored by ([a-zA-Z0-9_]{4,25})! \((\d+) Tokens - REGULAR Giveaway\) This is a promotion from CohhCarnage. Twitch does not sponsor or endorse broadcaster promotions and is not responsible for them. \*\*\*$/

  @refreshed ~r/^\s*\*\*\* Giveaway for (.+?): Sponsored by ([a-zA-Z0-9_]{4,25}) \((\d+) Tokens - REGULAR Giveaway\) closing in (ONE|TWO) minutes?, get your entries in by typing !enter if you have not already entered\. \[ID:(\d+)\] \*\*\*$/

  @finished ~r/^\s*\*\*\* The winner is: ([a-zA-Z0-9_]{4,25}) \*\*\* You have won (.+) donated by ([a-zA-Z0-9_]{4,25})\. You will be contacted via Twitch PM by a moderator. This is a promotion from CohhCarnage. Twitch does not sponsor or endorse broadcaster promotions and is not responsible for them. There were (\d+) entries for this giveaway\.$/

  def check_for_start(msg) do
    case Regex.run(@started, msg) do
      [_result, game, sponsor, prize] ->
        { :start, %{ "stage" => "start",
                       "game" => game,
                       "sponsor" => sponsor,
                       "tokens" => prize,
                     }
        }
      nil -> nil
    end
  end

  def check_for_refresh(msg) do
    case Regex.run(@refreshed, msg) do
      [_result, game, sponsor, tokens, minutes, _id] ->
        { :refresh, %{ "stage" => "refresh",
                       "game" => game,
                       "sponsor" => sponsor,
                       "tokens" => tokens,
                       "remaining_time" => minutes
                     }
        }
      nil -> nil
    end
  end

  def check_for_finish(msg) do
    case Regex.run(@finished, msg) do
      [_result, winner, game, sponsor, entries] ->
        { :finished, %{ "stage" => "finished",
                        "game" => game,
                        "winner" => winner,
                        "sponsor" => sponsor,
                        "entries" => entries
                     }
        }
      nil -> nil
    end
  end


end
