defmodule Cgas.Router do
  use Cgas.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    #plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Cgas do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  scope "/auth", Cgas do
    pipe_through :browser

    get "/", AuthController, :auth
    get "/callback", AuthController, :callback
  end

  scope "/giveaway", Cgas do
    pipe_through :browser

    post "/", GiveAwayController, :enter
  end

  # Other scopes may use custom stacks.
  # scope "/api", Cgas do
  #   pipe_through :api
  # end
end
