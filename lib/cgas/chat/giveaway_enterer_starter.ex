defmodule Cgas.GiveAwayEntererStarter do
  def enter(nick, token) do
    {:ok, pid } = Cgas.GiveAwayEnterer.enter(nick, token)
    pid
  end
end
