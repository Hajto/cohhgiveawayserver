defmodule Cgas.FCM do
  alias ExFCM.Message

  def push_giveaway(map) do
    %{"game" => game} = map
    Message.put_data(game)
    |> Message.put_notification("New Giveaway has opened!",
    game<>" is being given out!")
    |> Message.target_topic("giveaways")
    |> Message.send
  end

end
