defmodule Cgas.ChatSupervisor do
  use Supervisor

  def start_link do
    Supervisor.start_link(__MODULE__, :ok)
  end

  def init(:ok) do
    children = [
      worker(Cgas.GiveAwayMonitor,[])
     # worker(Cgas.GiveAwayEntererStarter, [])
    ]

    supervise(children, strategy: :one_for_one)
  end
end
