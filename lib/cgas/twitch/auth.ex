defmodule Cgas.TwitchAuth do
  use OAuth2.Strategy

  defmodule Token do
    @derive [Poison.Encoder]
    defstruct [:access_token]
  end

  # Public API

  def client do
    OAuth2.Client.new([
      strategy: __MODULE__,
      client_id: fetch_config_value(:client_id),
      client_secret: fetch_config_value(:client_secret),
      authorize_url: fetch_config_value(:auth_server),
      redirect_uri: fetch_config_value(:auth_callback),
      site: fetch_config_value(:site),
      token_url: "https://api.twitch.tv/kraken/oauth2/token"
    ])
  end

  def authorize_url! do
    OAuth2.Client.authorize_url!(client(), scope: "user_read chat_login")
  end

  def get_token!(params \\ [], headers \\ [], opts \\ []) do
    response = OAuth2.Client.get_token!(client(), params, headers, opts)
    IO.inspect response
    token_as_json = response.token.access_token
    case Poison.decode!(token_as_json, as: %Token{}).access_token do
      nil -> nil
      value -> wrapped =  %{ OAuth2.AccessToken.new(value) | token_type: "OAuth"  }
      %{ response | token: wrapped }
    end
  end

  # Strategy Callbacks

  def authorize_url(client, params) do
    OAuth2.Strategy.AuthCode.authorize_url(client, params)
  end

  def get_token(client, params, headers \\ []) do
    client
    |> put_header("accept", "application/json")
    |> put_param("client_secret", client.client_secret)
    |> OAuth2.Strategy.AuthCode.get_token(params, headers)
  end

  defp fetch_config_value(key), do: Application.fetch_env!(:cgas, key)
end
