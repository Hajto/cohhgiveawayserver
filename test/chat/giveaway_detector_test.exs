defmodule Cgas.GiveAwayDetectorTest do
  use Cgas.ConnCase

  @valid_start """
  *** NEW GIVEAWAY OPENED: (WWE 2k16): Sponsored by Tazmanianstitch! (1 Tokens - REGULAR Giveaway) This is a promotion from CohhCarnage. Twitch does not sponsor or endorse broadcaster promotions and is not responsible for them. ***
  """

  test " Finds giveway and extraxts data " do
    {_, map } = Cgas.GiveAwayMonitor.check_message @valid_start
    %{"game" => game,
      "sponsor" => donor,
      "tokens" => prize } = map
    assert game == "WWE 2k16"
    assert prize == "1"
    assert donor == "Tazmanianstitch"
  end

end
