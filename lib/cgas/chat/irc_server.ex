defmodule Cgas.IrcServer do
  defmacro __using__(opts) do
    quote do
      import Cgas.IrcServer
      Module.register_attribute __MODULE__, :handlers, accumulate: true

      @before_compile Cgas.IrcServer
      require Logger

      defmodule State do
        defstruct host: unquote(Keyword.get(opts, :host, "irc.chat.twitch.tv")),
          port: unquote(Keyword.get(opts, :port, 6667)),
          pass: unquote(Keyword.get(opts, :password, "password")),
          nick: unquote(Keyword.get(opts, :nick, "uname")),
          client: nil,
          handlers: [],
          channel: unquote(Keyword.get(opts, :channel, "#cohhcarnage"))
      end

      def start_link(state \\ %State{}) do
        {:ok, client} = ExIrc.start_client!
        GenServer.start_link(__MODULE__,
          [%{state | client: client}],
          name: unquote(Keyword.get(opts, :server_name, __CALLER__.module)))
      end

      def init([state]) do
        ExIrc.Client.add_handler state.client, self
        ExIrc.Client.connect! state.client, state.host, state.port
        {:ok, state}
      end

    end

  end

  defmacro expect_message(pattern, do: action) do
    quote bind_quoted: [
      pattern: Macro.escape(pattern, unquote: true),
      action: Macro.escape(action, unquote: true)
    ] do
      @handlers { pattern, action }
    end
  end

  defmacro __before_compile__(_env) do
    quote do
      #use GenServer
      Logger.debug "Compiling for #{__MODULE__}"

      def handle_info(:logged_in, config) do
        Logger.debug "Logged in succesfully"
        ExIrc.Client.join(config.client, config.channel)
        {:noreply, config}
      end

      def handle_info({:connected, server, port}, state) do
        Logger.debug(state.nick <> " " <> state.pass)
        logon_result = ExIrc.Client.logon(state.client, state.pass, state.nick, state.nick, state.nick)
        Logger.debug "logon" <> inspect(logon_result)
        {:noreply, state}
      end

      compile_handlers

      def handle_info(message, state) do
#        IO.inspect({:info, message})
        {:noreply, state}
      end

      def terminate(_, state) do
        # Quit the channel and close the underlying client connection when the process is terminating
        ExIrc.Client.quit state.client
        ExIrc.Client.stop! state.client
        Logger.debug "Quiting process for #{state.nick}"
        :ok
      end
    end
  end

  defmacro compile_handlers do
    suppress = quote do
      var!(state).client
    end
    Enum.map(Module.get_attribute(__CALLER__.module, :handlers), fn ({head , body}) ->
      quote do
        def handle_info(unquote(head), state_e) do
          var!(state) = state_e
          unquote(body)
          unquote(suppress)
          {:noreply, state_e }
        end
      end
    end)
  end

end
