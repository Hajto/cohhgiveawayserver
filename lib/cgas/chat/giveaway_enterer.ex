defmodule Cgas.GiveAwayEnterer do
  use Cgas.IrcServer,
    channel: "#hajtosek"

  def start(nick, password, state \\ %State{}) do
    {:ok, client} = ExIrc.start_client!
    GenServer.start(__MODULE__,
      [%{state | client: client,
         nick: nick,
         pass: password
        }]
    )
  end

  def enter(nick, token) do
    Logger.debug "Entering giveaway"
    start(nick, token)
  end

  def handle_info({:joined, _}, state) do
    ExIrc.Client.msg(state.client,
      :privmsg,
      state.channel,
      "!enter")
    {:stop, :normal, state}
  end
end
